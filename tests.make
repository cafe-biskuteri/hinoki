#!/usr/bin/env -S make -f

LIBS=-cp /usr/share/java/javax.json.jar:.

recompile:
	javac $(LIBS) $(wildcard cafe/biskuteri/hinoki/tests/*.java)

DSVTokeniser:
	java -ea cafe.biskuteri.hinoki.tests.DSVTokeniserTests

JsonConverter:
	java $(LIBS) -ea cafe.biskuteri.hinoki.tests.JsonConverterTests
	
PatternParser:
	java $(LIBS) -ea cafe.biskuteri.hinoki.tests.PatternParserTests

SilentParsers:
	java $(LIBS) -ea cafe.biskuteri.hinoki.tests.SilentParsersTests

.PHONY: recompile DSVTokeniser JsonConverter PatternParser SilentParsers
