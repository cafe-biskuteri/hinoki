/* copyright

This file is part of Hinoki.
Written in 2024 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki;

import java.io.Reader;
import java.io.IOException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

                                            public class
                                            DSVTokeniser {

                                        public static Tree<String>
                                        tokenise(
                                            Reader r, Options o)
                                        throws IOException
                                        {
Deque<Tree<String>> stack = new LinkedList<>();
stack.push(new Tree<String>());
StringBuilder value = new StringBuilder();
boolean escaped = false, rejectValue = false, finished = false;
int c; while (!finished)
{
    c = r.read();

    if (escaped)
    {
        value.append((char)c);
        escaped = false;
        continue;
        /*
        * Decided not to provide ignoring of escaped newlines.
        * Because, if a line is broken then further tabs or
        * spaces may follow. Better ask the client to trim
        * any field before using, or know where a line break
        * is going to occur. Or, do what I do, join lines
        * afterwards using key in first field.
        */
    }
    if (c == -1)
    {
        boolean noFieldsSoFar = stack.peek().size() == 0;
        boolean noValueSoFar = value.length() == 0;
        if (noFieldsSoFar && noValueSoFar)
        {
            value.append(o.endOfStreamValue);
            addValueLeaf(stack.peek(), value, false);
            break;
            /*
            * Don't move this break into the processor for finished.
            * That one will push the current value and if we break
            * there terminate the loop, yes, but the caller wants a
            * record with solely o.endOfStreamValue as their signal.
            * We need to send one such thing, so let us loop oncemore
            * after finished and reach this block where we know to do
            * that.
            */
        }
        c = o.stopper;
    }
    finished = c == o.stopper;
    if (c == '\\') { escaped = true; continue; }
    if (c == o.blockClose)
    {
        if (!(stack.size() > 1)) return null;

        if (!rejectValue)
		{
			addValueLeaf(
				stack.peek(), value, o.skipBlankFields);
		}
		else
		{
			if (value.length() == 0);
			else return null;
		}
		
        rejectValue = true;
        // Tell the next delimiter to push no value following
        // this block close.
        stack.pop();
		continue;
    }
    if (c == o.blockOpen)
    {
        if (value.length() != 0) return null;
        Tree<String> newBlock = new Tree<String>();
        stack.peek().add(newBlock);
        stack.push(newBlock);
		continue;
    }
    if (c == o.delimiter || finished)
    {
		if (!rejectValue)
		{
			if (value.toString().endsWith("\r"))
				value.deleteCharAt(value.length() - 1);
			addValueLeaf(
				stack.peek(), value, o.skipBlankFields);
		}
		else
		{
			if (value.length() == 0);
			else if (value.toString().equals("\r"));
			else return null;
		}

        rejectValue = false;
        continue;
    }
    value.append((char)c);
}

return stack.peekLast();
/*
* It'd be notable if not all blocks were closed before we
* reached the end of the line, but let's just let it slide,
* give the topmost block for this line, as though we inserted
* block closers ourselves.
*/
                                        }

                                        public static Check
                                        check(Reader r, Options o)
                                        throws IOException
                                        {
Check check = new Check();
check.position = 0;
check.character = 0;
check.description = null;
int level = 1;
boolean escaped = false, empty = false, rejectValue = false;
boolean finished = false;
int c; while (!finished)
{
    c = r.read();

    ++check.position;
    check.character = c;
    if (escaped)
    {
        empty = false;
        escaped = false;
        continue;
    }
    finished = c == o.stopper || c == -1;
    if (c == '\\') { escaped = true; continue; }
    if (c == o.blockOpen)
    {
        if (!empty) {
            check.description =
                "Blocks shouldn't be opened in the middle of a"
                + " non-empty field. There's no obvious place"
                + " in the tree to put that preceeding value.";
            return check;
        }
        ++level;
        continue;
    }
    if (c == o.blockClose)
    {
        --level;
        if (level < 1) {
            check.description =
                "There's too many block closers."
                + " Double-check your block levels!";
            return check;
        }
        empty = true;
        rejectValue = true;
        continue;
    }
    if (c == o.delimiter || finished)
    {
        empty = true;
        rejectValue = false;
        continue;
    }
    empty = false;
    if (rejectValue)
    {
        check.description =
            "A field shouldn't have characters once it has"
            + " a block. There's no obvious place in the"
            + " tree to put that succeeding value.";
        /*
        * We need this to have the new allowance for carriage
        * returns, somehow.
        */
        return check;
    }
}
return check;
                                        }

//                                      ---%-@-%---

                                        private static void
                                        addValueLeaf(
                                            Tree<String> tree,
                                            StringBuilder value,
                                            boolean skipBlankField)
                                        {
if (value.length() == 0 && skipBlankField) return;
Tree<String> leaf = new Tree<>();
leaf.value = value.toString();
tree.add(leaf);
value.delete(0, value.length());
                                        }

                                        private static boolean
                                        rejectableValue(StringBuilder value)
                                        {
if (value.length() == 0) return false;
if (value.toString().equals("\r")) return false;
return true;
                                        }

//                                      ---%-@-%---

                                        public static class
                                        Options {

public char
delimiter = ':',
blockOpen = '[',
blockClose = ']',
stopper = '\n';

public boolean
skipBlankFields = false;

public String
endOfStreamValue = "\031";
                                        }

                                        public static class
                                        Check {

public int
position, character;

public String
description;

                                        }

                                            }
