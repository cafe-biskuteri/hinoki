/* copyright

This file is part of Hinoki.
Written in 2024 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki.tests;

import cafe.biskuteri.hinoki.Tree;
import cafe.biskuteri.hinoki.SilentParsers;
import java.time.ZonedDateTime;

									public class
									SilentParsersTests {

								public static void
								testAll()
								{
testPrimitives();
testZonedDateTime();
								}

								public static void
								testPrimitives()
								{
Integer one = SilentParsers.toInt("401");
assert one.equals(401);

Integer two = SilentParsers.toInt("91238124891253123154");
assert two == null;

Double three = SilentParsers.toDouble("36.743");
assert three.equals(36.743);

Double four = SilentParsers.toDouble("6+2i");
assert four == null;

Long five = SilentParsers.toLong("922337203685477000");
assert five.equals(922337203685477000L);

Long six = SilentParsers.toLong("23.4");
assert six == null;

Boolean seven = SilentParsers.toBoolean("true");
assert seven.equals(true);

Boolean eight = SilentParsers.toBoolean("Nonsense");
assert eight == null;
								}

								public static void
								testZonedDateTime()
								{
ZonedDateTime one = SilentParsers
	.toZonedDateTime("2007-12-03T10:15:30+01:00[Europe/Paris]");
assert one != null;
								}

//								---%-@-%---

								public static void
								main(String... args)
								{ testAll(); }

}