/* copyright

This file is part of Hinoki.
Written in 2022 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki.tests;

import cafe.biskuteri.hinoki.JsonConverter;
import cafe.biskuteri.hinoki.Tree;
import java.io.StringReader;
import java.io.IOException;

                                            public class
                                            JsonConverterTests {

                                        public static void
                                        testMastodonV1Post()
                                        throws IOException
                                        {
StringReader r = new StringReader(
    "{"
    + "    \"id\": \"107999608989027587\","
    + "    \"created_at\": \"2022-03-22T10:00:52.397Z\","
    + "    \"in_reply_to_id\": null,"
    + "    \"in_reply_to_account_id\": null,"
    + "    \"sensitive\": false,"
    + "    \"spoiler_text\": \"\","
    + "    \"visibility\": \"unlisted\","
    + "    \"language\": \"ms\","
    + "    \"uri\": \"https://deadinsi.de/users/snowyfox/statuses/107999608989027587\","
    + "    \"url\": \"https://deadinsi.de/@snowyfox/107999608989027587\","
    + "    \"replies_count\": 0,"
    + "    \"reblogs_count\": 0,"
    + "    \"favourites_count\": 1,"
    + "    \"favourited\": false,"
    + "    \"reblogged\": false,"
    + "    \"muted\": false,"
    + "    \"bookmarked\": false,"
    + "    \"pinned\": false,"
    + "    \"content\": \"\\u003cp\\u003e🤗\\u003c/p\\u003e\","
    + "    \"reblog\": null,"
    + "    \"application\": {"
    + "        \"name\": \"Web\","
    + "        \"website\": null"
    + "    },"
    + "    \"account\": {"
    + "        \"id\": \"106943068059029316\","
    + "        \"username\": \"snowyfox\","
    + "        \"acct\": \"snowyfox\","
    + "        \"display_name\": \"\","
    + "        \"locked\": false,"
    + "        \"bot\": false,"
    + "        \"discoverable\": null,"
    + "        \"group\": false,"
    + "        \"created_at\": \"2021-09-16T00:00:00.000Z\","
    + "        \"note\": \"\\u003cp\\u003eAm a potato - in life; in work; and in society. Aspiring to be an elegant croissant..!\\u003c/p\\u003e\\u003cp\\u003eVoted sleepiest girl\\u003c/p\\u003e\\u003cp\\u003e杉原 宇砂鷲 (Usawashi Sugihara) (she/her)\\u003c/p\\u003e\","
    + "        \"url\": \"https://deadinsi.de/@snowyfox\","
    + "        \"avatar\": \"https://deadinside.sfo2.cdn.digitaloceanspaces.com/accounts/avatars/106/943/068/059/029/316/original/75ed9c64f4fe2383.png\","
    + "        \"avatar_static\": \"https://deadinside.sfo2.cdn.digitaloceanspaces.com/accounts/avatars/106/943/068/059/029/316/original/75ed9c64f4fe2383.png\","
    + "        \"header\": \"https://deadinside.sfo2.cdn.digitaloceanspaces.com/accounts/headers/106/943/068/059/029/316/original/1a784165af1e54cf.png\","
    + "        \"header_static\": \"https://deadinside.sfo2.cdn.digitaloceanspaces.com/accounts/headers/106/943/068/059/029/316/original/1a784165af1e54cf.png\","
    + "        \"followers_count\": 49,"
    + "        \"following_count\": 34,"
    + "        \"statuses_count\": 7039,"
    + "        \"last_status_at\": \"2022-03-22\","
    + "        \"emojis\": [],"
    + "        \"fields\": ["
    + "            {"
    + "                \"name\": \"Avatar\","
    + "                \"value\": \"From \\u0026apos;Dress Treat!\\u0026apos;\","
    + "                \"verified_at\": null"
    + "            }"
    + "        ]"
    + "    },"
    + "    \"media_attachments\": [],"
    + "    \"mentions\": [],"
    + "    \"tags\": [],"
    + "    \"emojis\": [],"
    + "    \"card\": null,"
    + "    \"poll\": null"
    + "}"
);

Tree<String> root = JsonConverter.convert(r);
assert root.value == null;
assert root.size() > 0;

Tree<String> id = root.get("id");
assert id != null;
assert id.size() == 0;
assert id.value.equals("107999608989027587");

Tree<String> language = root.get("language");
assert language != null;
assert language.size() == 0;
assert language.value.equals("ms");

Tree<String> account = root.get("account");
assert account != null;
assert account.value == null;
assert account.size() > 0;
Tree<String> acct = account.get("acct");
assert acct.value != null;
assert acct.value.equals("snowyfox");
Tree<String> note = account.get("note");
assert note.value != null;
assert note.value.equals(
    "<p>Am a potato - in life; in work; and in society. Aspiring to be an elegant croissant..!</p><p>Voted sleepiest girl</p><p>杉原 宇砂鷲 (Usawashi Sugihara) (she/her)</p>"
);

Tree<String> fields = account.get("fields");
assert fields.value == null;
assert fields.size() == 1;
Tree<String> field1 = fields.get(0);
assert field1.value == null;
assert field1.key.equals("0");
assert field1.size() == 3;
assert field1.get(0).key.equals("name");
assert field1.get(0).value.equals("Avatar");
assert field1.get(2).key.equals("verified_at");
assert field1.get(2).value == null;
                                        }

                                        public static void
                                        main(String... args)
                                        throws IOException
                                        {
testMastodonV1Post();
                                        }

                                            }
