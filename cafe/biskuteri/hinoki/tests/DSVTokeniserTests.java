/* copyright

This file is part of Hinoki.
Written in 2024 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki.tests;

import cafe.biskuteri.hinoki.DSVTokeniser;
import cafe.biskuteri.hinoki.Tree;
import java.io.StringReader;
import java.io.IOException;

                                            public class
                                            DSVTokeniserTests {

                                        public static void
                                        testLispCons()
                                        throws IOException
                                        {
DSVTokeniser.Options o = new DSVTokeniser.Options();
o.delimiter = ' '; o.skipBlankFields = true;
o.blockOpen = '('; o.blockClose = ')';
StringReader r = new StringReader("(cons ('salmon 'char))");

Tree<String> root = DSVTokeniser.tokenise(r, o);
assert root.size() == 1;
Tree<String> cons = root.get(0);
assert cons.size() == 2;
assert cons.get(1).size() == 2;
assert cons.get(1).get(0).value.equals("'salmon");
assert cons.get(1).get(1).value.equals("'char");
                                        }


                                        public static void
                                        testWeirdLisp()
                                        throws IOException
                                        {
DSVTokeniser.Options o = new DSVTokeniser.Options();
o.delimiter = ' '; o.skipBlankFields = true;
o.blockOpen = '('; o.blockClose = ')';

StringReader r = new StringReader("within list ( defun ())princ");
DSVTokeniser.Check check = DSVTokeniser.check(r, o);
assert check.description != null;
assert check.position == 24;
assert check.character == 'p';
/*
* This differs from most Lisp environments. They opt to use our
* previous behaviour, pushing preceeding and succeeding text as
* new fields. But we're not sure if that's what a user of Hinoki
* would intend when they write something like this.
*
* I keep with the reasoning in my error handling experiment.
* This tokeniser is compound-DSV-first, and I want the format
* to be simple, rather than allow input like this.
*/

r = new StringReader("within list ( defun ()) princ");
Tree<String> root = DSVTokeniser.tokenise(r, o);
assert root.size() == 4;
assert root.get(3).value.equals("princ");
Tree<String> cons = root.get(2);
assert cons.size() == 2;
assert cons.get(0).value.equals("defun");
assert cons.get(1).value == null;
assert cons.get(1).size() == 0;
                                        }

                                        public static void
                                        testBlocks()
                                        throws IOException
                                        {
StringReader r1 = new StringReader(":[:]:[[[]:]:]");
StringReader r2 = new StringReader(":[:]:[[[]:]:]");
//                                 1 2   3
//                                   1 2  1     2
//                                        1  2
//                                         12
DSVTokeniser.Options o = new DSVTokeniser.Options();

assert DSVTokeniser.check(r1, o).description == null;
Tree<String> root = DSVTokeniser.tokenise(r2, o);
assert root.size() == 3;
assert root.get(0).size() == 0;

assert root.get(1).size() == 2;
assert root.get(1).get(0).value.equals("");
assert root.get(1).get(1).value.equals("");

assert root.get(2).size() == 2;
assert root.get(2).get(0).size() == 2;
assert root.get(2).get(0).get(0).size() == 1;
assert root.get(2).get(0).get(0).get(0).value.equals("");
assert root.get(2).get(0).get(1).value.equals("");
assert root.get(2).get(1).value.equals("");
                                        }

                                        public static void
                                        testEscapes()
                                        throws IOException
                                        {
StringReader r1 = new StringReader("one\\::two:\\[three:\\]four");
StringReader r2 = new StringReader("one\\::two:\\[three:\\]four");
DSVTokeniser.Options o = new DSVTokeniser.Options();

DSVTokeniser.Check c = DSVTokeniser.check(r1, o);
if (c.description != null) {
    assert false;
}

Tree<String> root = DSVTokeniser.tokenise(r2, o);
assert root.size() == 4;
assert root.get(0).value.equals("one:");
assert root.get(1).value.equals("two");
assert root.get(2).value.equals("[three");
assert root.get(3).value.equals("]four");
                                        }

                                        public static void
                                        testPlain()
                                        throws IOException
                                        {
StringReader r;
DSVTokeniser.Options o = new DSVTokeniser.Options();

r = new StringReader("carrot:100g:RM5");
assert DSVTokeniser.check(r, o).description == null;

r = new StringReader("carrot:100g:RM5");
Tree<String> root = DSVTokeniser.tokenise(r, o);
assert root.size() == 3;
assert root.get(0).value.equals("carrot");
assert root.get(1).value.equals("100g");
assert root.get(2).value.equals("RM5");
                                        }

                                        public static void
                                        testStandard()
                                        throws IOException
                                        {
StringReader r;
DSVTokeniser.Options o = new DSVTokeniser.Options();

r = new StringReader("carrot:[[100g:5]:[1kg:15]]:[organic]:Rainbow");
assert DSVTokeniser.check(r, o).description == null;

r = new StringReader("carrot:[[100g:5]:[1kg:15]]:[organic]:Rainbow");
Tree<String> root = DSVTokeniser.tokenise(r, o);
assert root.size() == 4;
assert root.get(3).size() == 0;
assert root.get(2).size() == 1;
assert root.get(1).size() == 2;
assert root.get(1).get(0).get(0).value.equals("100g");
assert root.get(1).get(0).get(1).value.equals("5");
assert root.get(1).get(1).get(0).value.equals("1kg");
assert root.get(1).get(1).get(1).value.equals("15");
                                        }

                                        public static void
                                        testNewlineEscape()
                                        throws IOException
                                        {
StringReader r;
DSVTokeniser.Options o = new DSVTokeniser.Options();

r = new StringReader("newline:\\\nseparated");
assert DSVTokeniser.check(r, o).description == null;

r = new StringReader("newline:\\\nseparated");
Tree<String> root = DSVTokeniser.tokenise(r, o);
assert root.size() == 2;
assert root.get(1).value.equals("\nseparated");
                                        }

                                        public static void
                                        testNewline()
                                        throws IOException
                                        {
StringReader r = new StringReader("one:record\ntwo:record");
DSVTokeniser.Options o = new DSVTokeniser.Options();

Tree<String> one = DSVTokeniser.tokenise(r, o);
assert one.size() == 2;
assert one.get(1).value.equals("record");
Tree<String> two = DSVTokeniser.tokenise(r, o);
assert two.size() == 2;
assert two.get(0).value.equals("two");
                                        }

                                        public static void
                                        testEndOfStream()
                                        throws IOException
                                        {
StringReader r = new StringReader("field");
DSVTokeniser.Options o = new DSVTokeniser.Options();

Tree<String> one = DSVTokeniser.tokenise(r, o);
assert one != null;
assert one.size() == 1;
assert one.get(0).value.equals("field");

Tree<String> two = DSVTokeniser.tokenise(r, o);
assert two != null;
assert two.size() == 1;
assert two.get(0).value.length() != 0;
assert two.get(0).value.equals(o.endOfStreamValue);
                                        }

                                        public static void
										testBlocksWithCR()
                                        throws IOException
										{
StringReader r = new StringReader("value1:value2:[field1:field2]\r\n");
DSVTokeniser.Options o = new DSVTokeniser.Options();

Tree<String> one = DSVTokeniser.tokenise(r, o);
assert one != null;
assert one.size() == 3;
assert one.get(2).get(0).value.equals("field1");
assert one.get(2).get(1).value.equals("field2");

Tree<String> two = DSVTokeniser.tokenise(r, o);
assert two != null;
assert two.size() == 1;
assert two.get(0).value.equals(o.endOfStreamValue);
										}

                                        public static void
										testNewlineWithCR()
                                        throws IOException
										{
StringReader r = new StringReader("Line1\r\nLine2\r\n");
DSVTokeniser.Options o = new DSVTokeniser.Options();

Tree<String> one = DSVTokeniser.tokenise(r, o);
Tree<String> two = DSVTokeniser.tokenise(r, o);
assert one != null;
assert two != null;
assert one.size() == 1;
assert two.size() == 1;
assert one.get(0).value.equals("Line1");
assert two.get(0).value.equals("Line2");

Tree<String> three = DSVTokeniser.tokenise(r, o);
assert three != null;
assert three.size() == 1;
assert three.get(0).value.equals(o.endOfStreamValue);
										}


                                        public static void
                                        testAll()
                                        throws IOException
                                        {
testLispCons();
testWeirdLisp();
testBlocks();
testEscapes();
testPlain();
testStandard();
testNewlineEscape();
testNewline();
testBlocksWithCR();
testNewlineWithCR();
testEndOfStream();
                                        }

//                                        ---%-@-%---

                                        public static void
                                        main(String... args)
                                        throws IOException
                                        {
                                            testAll();
                                        }

                                            }
