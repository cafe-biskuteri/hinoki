/* copyright

This file is part of Hinoki.
Written in 2023 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki.tests;

import cafe.biskuteri.hinoki.PatternParser;
import cafe.biskuteri.hinoki.Tree;
import java.util.List;
import java.util.ArrayList;

									class
									PatternParserTests {

								public static void
								main(String... args)
								{
testNoPatterns();
testQuotedAndBracketed();
testAdjacent();
testDangerousQuotes();
testKeywords();
								}

								public static void
								testNoPatterns()
								{
String string =
    "Some random text, even with _special_ " +
    "looking[a] tokens.";

List<Tree<String>> tokens = PatternParser
    .parse(string, new ArrayList<>());
assert tokens != null;
assert tokens.size() == 1;
assert tokens.get(0).key.equals("text");
assert tokens.get(0).value.equals(string);
								}

								public static void
								testQuotedAndBracketed()
								{
String string = "(bracketed) and \"quoted\" words here.";

class Brackets implements PatternParser.Pattern {

    public String
    patternName() { return "bracketed"; }

    public int
    patternRecognise(String string, int so)
    {
        if (string.charAt(so) != '(') return -1;
        int eo = string.indexOf(')', so);
        return eo == -1 ? -1 : eo + 1;
    }

}
class Quotes implements PatternParser.Pattern {

    public String
    patternName() { return "quoted"; }

    public int
    patternRecognise(String string, int so)
    {
        if (string.charAt(so) != '"') return -1;
        int eo = string.indexOf('"', so + 1);
        return eo == -1 ? -1 : eo + 1;
    }

}
List<PatternParser.Pattern> patterns = new ArrayList<>();
patterns.add(new Brackets());
patterns.add(new Quotes());

List<Tree<String>> tokens = PatternParser
    .parse(string, patterns);
assert tokens.size() == 4;
assert tokens.get(0).key.equals("bracketed");
assert tokens.get(0).value.equals("(bracketed)");
assert tokens.get(1).key.equals("text");
assert tokens.get(1).value.equals(" and ");
assert tokens.get(2).key.equals("quoted");
assert tokens.get(2).value.equals("\"quoted\"");
assert tokens.get(3).key.equals("text");
assert tokens.get(3).value.equals(" words here.");
								}

								public static void
								testAdjacent()
								{
String string = "AdjacentTitleCaseWords";

class Title implements PatternParser.Pattern {

    public String
    patternName() { return "TitleCase"; }

    public int
    patternRecognise(String string, int so)
    {
        char c = string.charAt(so);
        if (!Character.isUpperCase(c)) return -1;

        int eo;
        for (eo = so + 1; eo < string.length(); ++eo)
        {
            c = string.charAt(eo);
            if (!Character.isLowerCase(c)) break;
        }
        return eo;
    }

}
List<PatternParser.Pattern> patterns = new ArrayList<>();
patterns.add(new Title());

List<Tree<String>> tokens = PatternParser
    .parse(string, patterns);
assert tokens.size() == 4;
assert tokens.get(0).key.equals("TitleCase");
assert tokens.get(1).key.equals("TitleCase");
assert tokens.get(2).key.equals("TitleCase");
assert tokens.get(3).key.equals("TitleCase");
assert tokens.get(0).value.equals("Adjacent");
assert tokens.get(1).value.equals("Title");
assert tokens.get(2).value.equals("Case");
assert tokens.get(3).value.equals("Words");
								}

								public static void
								testDangerousQuotes()
								{
String string = "name=\"'\" id='\"' another='\"'";

class DoubleQuotes implements PatternParser.Pattern {

    public String
    patternName() { return "double"; }

    public int
    patternRecognise(String string, int so)
    {
        if (string.charAt(so) != '"') return -1;
        int eo = string.indexOf('"', so + 1);
        return eo == -1 ? -1 : eo + 1;
    }

}
class SingleQuotes implements PatternParser.Pattern {

    public String
    patternName() { return "single"; }

    public int
    patternRecognise(String string, int so)
    {
        if (string.charAt(so) != '\'') return -1;
        int eo = string.indexOf('\'', so + 1);
        return eo == -1 ? -1 : eo + 1;
    }

}
List<PatternParser.Pattern> patterns = new ArrayList<>();
patterns.add(new DoubleQuotes());
patterns.add(new SingleQuotes());

List<Tree<String>> tokens = PatternParser
    .parse(string, patterns);
assert tokens.get(0).key.equals("text");
assert tokens.get(0).value.equals("name=");
assert tokens.get(1).key.equals("double");
assert tokens.get(1).value.equals("\"'\"");
assert tokens.get(2).key.equals("text");
assert tokens.get(2).value.equals(" id=");
assert tokens.get(3).key.equals("single");
assert tokens.get(3).value.equals("'\"'");
assert tokens.get(4).key.equals("text");
assert tokens.get(4).value.equals(" another=");
assert tokens.get(5).key.equals("single");
assert tokens.get(5).value.equals("'\"'");
								}

								public static void
								testKeywords()
								{
String string =
    "Tubers are collected and cooked as part of " +
    "local cuisine in many parts of the world. Most " +
    "popular is the potato[1], which needs no " +
    "introduction by us. Taro[2] is found in East " +
    "Asia and the islands of the Pacific Ocean and " +
    "more, and becomes soft quickly upon boiling. " +
    "And various vegetables simply named 'yam'[3] " +
    "can be found in markets in Africa and South Asia " +
    "or markets for their diasporae.";

class Tubers implements PatternParser.Pattern {

    public String
    patternName() { return "tuber"; }

    public int
    patternRecognise(String string, int so)
    {
        if (string.startsWith("potato", so))
            return so + 6;
        if (string.startsWith("taro", so))
            return so + 4;
        if (string.startsWith("yam", so))
            return so + 3;
        if (string.startsWith("Potato", so))
            return so + 6;
        if (string.startsWith("Taro", so))
            return so + 4;
        if (string.startsWith("Yam", so))
            return so + 3;
        return -1;
    }

}
List<PatternParser.Pattern> patterns = new ArrayList<>();
patterns.add(new Tubers());

List<Tree<String>> tokens = PatternParser
	.parse(string, patterns);

int n = 0;
for (Tree<String> token: tokens)
    if (token.key.equals("tuber")) ++n;
assert n == 3;
								}

									}
