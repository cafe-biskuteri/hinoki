/* copyright

This file is part of Hinoki.
Written in 2024 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;

                                    public class
                                    Tree<R>
                                    implements Iterable<Tree<R>> {

public R
value;

public String
key;

public final List<Tree<R>>
children = new LinkedList<>();

//                              ---%-@-%---

                                public boolean
                                add(Tree<R> child)
                                {
return children.add(child);
                                }

                                public boolean
                                add(String key, R value)
                                {
return children.add(new Tree<R>(key, value));
                                }

                                public Tree<R>
                                get(int offset)
                                {
return children.get(offset);
                                }

                                public Tree<R>
                                get(String key)
                                {
for (Tree<R> c: children) if (c.key.equals(key)) return c;
return null;
                                }

                                public boolean
                                remove(Tree<R> child)
                                {
return children.remove(child);
                                }

                                public int
                                size()
                                {
return children.size();
                                }

//                               -    -%-     -

                                public Iterator<Tree<R>>
                                iterator()
                                {
return children.iterator();
                                }

                                public String
                                toString()
                                {
return getClass().getName()
    + "[key=" + key
    + ",value=" + value
    + ",children=" + size() + "]";
                                }

//                              ---%-@-%---

                                public
                                Tree() { }

                                public
                                Tree(String key, R value)
                                {
this.key = key;
this.value = value;
                                }

                                    }
