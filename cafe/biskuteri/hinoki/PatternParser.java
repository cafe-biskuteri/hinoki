/* copyright

This file is part of Hinoki.
Written in 2023 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki;

import java.util.List;
import java.util.ArrayList;

public class
PatternParser {
    
    public static List<Tree<String>>
    parse(String string, List<Pattern> patterns)
    {
        assert string != null;
        assert patterns != null;
        
        List<Tree<String>> returnee = new ArrayList<>();
        
        int so1 = 0, so2 = so1; while (so2 < string.length())
        {
            Pattern recognisedPattern = null;
            int endingOffset = -1;
            for (Pattern pattern: patterns)
            {
                int endingOffset2 = pattern
                    .patternRecognise(string, so2);
                if (endingOffset2 == -1) continue;
                assert endingOffset2 > so2;
                assert endingOffset2 <= string.length();
                
                recognisedPattern = pattern;
                endingOffset = endingOffset2;
            }
            
            if (recognisedPattern == null)
            {
                ++so2;
                continue;
            }
            
            assert so2 >= so1;
            if (so2 != so1)
            {
                Tree<String> addee = new Tree<>();
                addee.key = "text";
                addee.value = string.substring(so1, so2);
                returnee.add(addee);
                so1 = so2;
            }
            
            Tree<String> addee = new Tree<>();
            addee.key = recognisedPattern.patternName();
            addee.value = string.substring(so2, endingOffset);
            returnee.add(addee);
            so1 = so2 = endingOffset;
        }
        
        assert so2 == string.length();
        if (so2 != so1)
        {
            Tree<String> addee = new Tree<>();
            addee.key = "text";
            addee.value = string.substring(so1, so2);
            returnee.add(addee);
            so1 = so2;
        }
        
        return returnee;
    }
    
//    ---%-@-%---
    
    public interface
    Pattern {
        
        String
        patternName();
        
        int
        patternRecognise(String string, int startingOffset);
        
    }
    
}

