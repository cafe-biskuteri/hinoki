/* copyright

This file is part of Hinoki.
Written in 2022 by Usawashi <usawashi16@yahoo.co.jp>

To the extent possible under law, the author has dedicated
all copyright and related and neighboring rights to this file to
the public domain worldwide.

This file is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this file. If not,
see <http://creativecommons.org/publicdomain/zero/1.0/>.

copyright */

package cafe.biskuteri.hinoki;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonNumber;
import javax.json.JsonString;
import javax.json.stream.JsonParsingException;
import java.io.Reader;
import java.io.IOException;

                                            public class
                                            JsonConverter {

                                        public static Tree<String>
                                        convert(Reader r)
                                        throws IOException
                                        {
try {
    Tree<String> root = new Tree<>();
    fillValues(root, Json.createReader(r).read());
    return root;
}
catch (JsonParsingException eJp) {
    throw new IOException(eJp);
}
                                        }

                                        public static void
                                        fillValues(
                                            Tree<String> node,
                                            JsonValue value)
                                        throws IOException
                                        {
if (value instanceof JsonString) {
    node.value = ((JsonString)value).getString();
}
if (value instanceof JsonNumber) {
    double n = ((JsonNumber)value).doubleValue();
    node.value = Double.toString(n);
}
if (value instanceof JsonArray) {
    JsonArray a = (JsonArray)value;
    for (int o = 0; o < a.size(); ++o) {
        Tree<String> leaf = new Tree<String>();
        leaf.key = Integer.toString(o);
        fillValues(leaf, a.get(o));
        node.add(leaf);
    }
}
if (value instanceof JsonObject) {
    JsonObject o = (JsonObject)value;
    for (String key: o.keySet()) {
        Tree<String> leaf = new Tree<String>();
        leaf.key = key;
        fillValues(leaf, o.get(key));
        node.add(leaf);
    }
}
if (value == JsonValue.TRUE) {
    node.value = "true";
}
if (value == JsonValue.FALSE) {
    node.value = "false";
}
if (value == JsonValue.NULL) {
    node.value = null;
}
                                        }

                                            }
