
package cafe.biskuteri.hinoki;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

                                    public class
                                    SilentParsers {

                                public static Integer
                                toInt(String serialisation)
                                {
if (serialisation == null) return null;
try { return Integer.parseInt(serialisation); }
catch (NumberFormatException eNf) { return null; }
                                }

                                public static Double
                                toDouble(String serialisation)
                                {
if (serialisation == null) return null;
try { return Double.parseDouble(serialisation); }
catch (NumberFormatException eNf) { return null; }
                                }

                                public static Long
                                toLong(String serialisation)
                                {
if (serialisation == null) return null;
try { return Long.parseLong(serialisation); }
catch (NumberFormatException eNf) { return null; }
                                }

                                public static Boolean
                                toBoolean(String serialisation)
                                {
if (serialisation == null) return null;
String s = serialisation.toUpperCase();
if (!(s.equals("TRUE") || s.equals("FALSE"))) { return null; }
else { return Boolean.parseBoolean(serialisation); }
                                }

                                public static ZonedDateTime
                                toZonedDateTime(String serialisation)
                                {
if (serialisation == null) return null;
try { return ZonedDateTime.parse(serialisation); }
catch (DateTimeParseException eDtp) { return null; }
                                }

                                public static String
                                toString(String serialisation)
                                {
return serialisation;
                                }

                                    }
